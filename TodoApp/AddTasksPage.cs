﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using Dell.Adept.Core;
using Dell.Adept.UI.Web.Pages;
using Dell.Adept.UI.Web.Support.Extensions.WebDriver;
using Dell.Adept.UI.Web.Support.Extensions.WebElement;
using System.Collections.ObjectModel;
using System.Threading;
using System.Linq;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace TodoApp
{
    class AddTasksPage : PageBase
    {
        IWebDriver webDriver;
        int timeout = 120;

        /// <summary>
        /// Constructor to hand off webDriver
        /// </summary>
        /// <param name="webDriver"></param>
        public AddTasksPage(IWebDriver webDriver) : base(ref webDriver)
        {   
            this.webDriver = webDriver;
            webDriver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(timeout));
            webDriver.Manage().Window.Maximize();
            PageFactory.InitElements(webDriver, this);
        }

        [FindsBy(How = How.XPath, Using = @"//*[text() = 'Sign In']")]
        private IWebElement signInTab { get; set; }

        [FindsBy(How = How.Id, Using = @"user_email")]
        private IWebElement userName { get; set; }

        [FindsBy(How = How.Id, Using = @"user_password")]
        private IWebElement password { get; set; }

        [FindsBy(How = How.Name, Using = @"commit")]
        private IWebElement signIn { get; set; }

        [FindsBy(How = How.XPath, Using = @"//*[text() = 'My Tasks']")]
        private IWebElement myTasks { get; set; }

        [FindsBy(How = How.Id, Using = @"new_task")]
        private IWebElement taskDescription { get; set; }

        [FindsBy(How = How.XPath, Using = @"//span[@class='input-group-addon glyphicon glyphicon-plus']")]
        private IWebElement addButton { get; set; }

        [FindsBy(How = How.XPath, Using = @"table[@class='table']")]
        private IWebElement tasksList { get; set; }
        
        [FindsBy(How = How.XPath, Using = @"//*[text() = 'Task description should be at least three characters long.']")]
        private IWebElement minimumTaskCharsMsg { get; set; }

        [FindsBy(How = How.Id, Using = @"new_sub_task")]
        private IWebElement subTaskDescription { get; set; }

        [FindsBy(How = How.Id, Using = @"dueDate")]
        private IWebElement dueDate { get; set; }

        [FindsBy(How = How.Id, Using = @"add-subtask")]
        private IWebElement addSubTask { get; set; }

        [FindsBy(How = How.XPath, Using = @"//*[text() = 'Close']")]
        private IWebElement close { get; set; }

        /// <summary>
        /// Navigate to Todo App home page.
        /// </summary>
        /// <param name="helixURL">Helix home page address. e.g.: http://ausuwcaapps3.aus.amer.dell.com:7800</param>
        public void GoToTodoAppHomePage(string todoAppURL)
        {
            webDriver.Navigate().GoToUrl(todoAppURL);
            webDriver.SwitchTo().Frame("content");
        }

        /// <summary>
        /// Perform login action.
        /// </summary>
        /// <param name="login">Username.</param>
        /// <param name="pwd">Password.</param>
        public void Login(string login, string pwd)
        {
            signInTab.Click();
            userName.SendKeys(login);
            password.SendKeys(pwd);
            signIn.Click();
        }

        /// <summary>
        /// Inform task description and create new task.
        /// </summary>
        /// <param name="description">Task description.</param>
        public void AddTask(string description)
        {
            taskDescription.SendKeys(description);
            addButton.Click();
        }

        /// <summary>
        /// Inform task description.
        /// </summary>
        /// <param name="description">Task description.</param>
        public void EnterTaskDescription(string description)
        {
            taskDescription.SendKeys(description);
        }

        /// <summary>
        /// Return description size.
        /// </summary>
        public int GetDescriptionSize()
        {
            return taskDescription.GetAttribute("text").Length;
        }

        /// <summary>
        /// Wait for message to be displayed.
        /// </summary>
        public void WaitForMinimumAmountOfCharsMessage()
        {
            minimumTaskCharsMsg.IsElementVisible();
        }

        /// <summary>
        /// Return amount of rows on tasks list.
        /// </summary>
        public int GetAmountOfTasks()
        {
            return webDriver.FindElements(By.XPath("table[@class='table']/tbody/tr")).Count;
        }

        /// <summary>
        /// Return amount of rows on tasks list.
        /// </summary>
        /// <param name="description">Subtask description.</param>
        /// <param name="dueDateValue">Subtask due date ().</param>
        /// <param name="taskToBeManaged">Index of the task to be managed (where sub task is going to be created).</param>
        public void AddSubTask(string description, string dueDateValue, int taskToBeManaged = 0)
        {
            //Click on manage task - if father task has not been informed (taskToBeManaged = 0), select the first task
            if (taskToBeManaged == 0)
                webDriver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button"));
            else
                webDriver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[" + taskToBeManaged + "]/td[4]/button"));

            //Wait for modal to be displayed
            while (webDriver.WindowHandles.Count < 2)
                Thread.Sleep(1000);

            // Store current page to get back to it after closing pop-up message(s)
            string toDoApp = webDriver.CurrentWindowHandle;
            ReadOnlyCollection<string> browserWindows = webDriver.WindowHandles;

            // Find modal
            foreach (string dialog in browserWindows)
            {
                if (dialog != toDoApp)
                {
                    // Change focus to modal
                    webDriver.SwitchTo().Window(dialog);

                    // Enter description, due date and hit Close
                    subTaskDescription.SendKeys(description);
                    dueDate.SendKeys(dueDateValue);
                    addSubTask.Click();
                    close.Click();
                }
            }

            // Set focus back to ToDo App page
            webDriver.SwitchTo().Window(toDoApp);
            webDriver.SwitchTo().Frame("content");
        }
    }
}
