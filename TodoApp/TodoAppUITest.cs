﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentAssertions;
using Dell.Adept.UI.Web.Testing.Framework;
using Dell.Adept.UI.Web.Testing.Framework.WebDriver;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TodoApp
{
    /// <summary>
    /// Summary description for DellWebUITest
    /// </summary>
    [TestClass]
    [SingleWebDriver(false)]
    public class TodoAppUITest : WebUIMsTestBase
    {
        /// <summary>
        /// Initializes a new instance of the TodoAppUITest class.
        /// </summary>
        public TodoAppUITest() {}

        /// <summary>
        /// Set WebDriver.
        /// </summary>
        AddTasksPage AddTasksPage
        {
            get
            {
                return new AddTasksPage(TestWebDriver);
            }
        }

        /// <summary>
        /// Check if it is possible to add a task with less than three characters.
        /// </summary>
        [TestMethod]
        [Owner("J_L_Santos")]
        [Timeout(TestTimeout.Infinite)]
        [Priority(2)]
        [TestCategory("")]
        [Description("Check if it is possible to add a task with less than three characters.")]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML","|DataDirectory|\\TestDataSource.xml","TestData", DataAccessMethod.Sequential)] 
        //[DeploymentItem(@"FromSolutionFolderName\TestDataSource.xml", "OutputFolderName")]
        public void checkDescriptionMinimumSize()
        {
            AddTasksPage.GoToTodoAppHomePage("http://qa-test.avenuecode.com/users/sign_up");
            AddTasksPage.Login("zluis182@hotmail.com", "4venueCode");
            AddTasksPage.AddTask("AB");
            AddTasksPage.WaitForMinimumAmountOfCharsMessage();
        }

        /// <summary>
        /// Check if it is possible enter more than 250 characters on description field.
        /// </summary>
        [TestMethod]
        [Owner("J_L_Santos")]
        [Timeout(TestTimeout.Infinite)]
        [Priority(2)]
        [TestCategory("")]
        [Description("Check if it is possible enter more than 250 characters on description field.")]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML","|DataDirectory|\\TestDataSource.xml","TestData", DataAccessMethod.Sequential)] 
        //[DeploymentItem(@"FromSolutionFolderName\TestDataSource.xml", "OutputFolderName")]
        public void checkDescriptionMaximumSize()
        {
            AddTasksPage.GoToTodoAppHomePage("http://qa-test.avenuecode.com/users/sign_up");
            AddTasksPage.Login("zluis182@hotmail.com", "4venueCode");
            AddTasksPage.EnterTaskDescription("US1 - Minimum number of characters is not being checked when hitting EnterIssue:Users are able to create tasks when entering less than three characters on task description and hitting <Enter>. However, tasks should require at least three charactersX");
            Assert.AreEqual(AddTasksPage.GetDescriptionSize(), 250);
        }

        /// <summary>
        /// Add a subtask.
        /// </summary>
        [TestMethod]
        [Owner("J_L_Santos")]
        [Timeout(TestTimeout.Infinite)]
        [Priority(2)]
        [TestCategory("")]
        [Description("Check if it is possible enter more than 250 characters on description field.")]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML","|DataDirectory|\\TestDataSource.xml","TestData", DataAccessMethod.Sequential)] 
        //[DeploymentItem(@"FromSolutionFolderName\TestDataSource.xml", "OutputFolderName")]
        public void addSubTask()
        {
            AddTasksPage.GoToTodoAppHomePage("http://qa-test.avenuecode.com/users/sign_up");
            AddTasksPage.Login("zluis182@hotmail.com", "4venueCode");
            AddTasksPage.AddSubTask("XY", "12/20/2016");
        }

    }
}

